"use strict";
// onde estao as varias camadas do cenario
class nivelBar_menu extends SpriteImage_menu
{

	constructor (x,y,w,h,cor,numero,estado)
	{
		super(x,y,w,h,null);
		this.cor = cor;
		this.numero = numero;
		this.estado = estado;

	}

	draw(ctx){	
		for(let i = 0;i<this.numero;i++){
			if(i<this.estado){
				ctx.fillStyle = this.cor+"1)";
			}else{
				ctx.fillStyle = this.cor+"0.15)";
			}
			ctx.fillRect(this.x+(this.width+10)*i,this.y,this.width,this.height);
		}
	}
	adiciona(){
		this.estado+=1;
		if(this.estado>this.numero){
			this.estado = this.numero;
		}
	}
	subtrai(){
		this.estado-=1;
		if(this.estado<0){
			this.estado = 0;
		}
	}
	dentro(posX,posY){
		return false;
	}
}