"use strict";
//blocos estáticos do jogo
class Blocos extends SpriteImage
{
	constructor (x,y,cor)
	{
		//tamanho blocos 
		var w = 30;
		var h = 30;

		//para criar o image data
		var c = document.createElement("canvas");
		var ctx1 = c.getContext("2d");
		ctx1.fillStyle = cor;
		ctx1.fillRect(0,0,w,h);
		var img =  ctx1.getImageData(0,0,w,h);
		//fim da criacao do image data

		super(x,y,w,h,img);
	}

	//funçoes de desanho e de clear são executadas pelo pai
	draw(ctx){super.colocaImage(ctx);}
	clear(ctx){super.clear(ctx);}
	update(ctx){

	}

}