"use strict";

class menu_creditos{

	constructor(canvas,ctx){
		this.canvas = canvas;
		this.ctx = ctx;
		this.spArray;
	}

	carrega(){
		this.init(this.ctx);	
	}

	init(ctx){
		var spArray = [];
		var img = new Image();
		img.addEventListener("load",imgLoadHandler);
		img.id = "fundo";
		img.src = "imagens/fundo_creditos.png";

		function imgLoadHandler(ev){
			ev.target.removeEventListener("load",imgLoadHandler);
			var cenario = new SpriteImage_menu(0,0,1000,600,ev.target);
			spArray.push(cenario);

			var creditos = new Image();
			creditos.addEventListener("load",creditosLoadHandler);
			creditos.id = "creditos";
			creditos.src = "imagens/creditos.png";	
		}

		function creditosLoadHandler(ev){
			ev.target.removeEventListener("load",creditosLoadHandler);
			var creditos = new SpriteImage_menu(100,100,800,400,ev.target);
			spArray.push(creditos);

			var botao = new Botao_menu(950,20,30,30,"rgba(139,69,19,","X");
			spArray.push(botao);

			var ev2 = new Event("comecar");
			ev2.spArray = spArray;
			ctx.canvas.dispatchEvent(ev2);
		}
		
	}

	draw()
	{
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			this.spArray[i].draw(this.ctx);
		}
	}

	//funcao responsavel por actualizar as posicoes
	render()
	{
		this.draw();
	}
	mouseMove(posX,posY){
		var teste = false;
		var dim = this.spArray.length;

		for (let i = 0; i < dim; i++){
			if(this.spArray[i].dentro(posX,posY)!=""){
				teste = true;
			}
		}

		if (teste){
			document.getElementById("canvas").style.cursor = "pointer";
		}else{
			document.getElementById("canvas").style.cursor = "initial";
		}
	}

	mouseClick(posX,posY){
		var botao = "";
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			botao = this.spArray[i].dentro(posX,posY);
			if(botao != ""){
				break;
			}
		}
		if(botao=="X"){
			var ev3 = new Event("creditos_sair");
			this.ctx.canvas.dispatchEvent(ev3);
		}
	}

}
	