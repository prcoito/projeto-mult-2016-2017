"use strict";
// personagem controlada pelo utlizador

class Personagem extends SpriteImage
{
	constructor (x,y,w,h,tipo, health, speed, alpha,limite)
	{
		super(x,y,w,h,null);

		this.tipo = tipo;
		this.speed = speed;
		this.alpha = alpha;
		
		this.estado = 0;
		this.Aux1 = 0;
		this.Aux2 = 0;

		this.healthIni = health;
		this.health = health;

		this.index = 0;
		this.num_index = 0;
		this.dir = 1;
		this.img = [];
		this.imgData = [];

		this.collected = 0;

		this.gravidade = 1;
		this.salto = 20;
		this.teste_salto = false;

		//teclas
		this.keyU = false;
		this.keyD = false;
		this.keyL = false;
		this.keyR = false;

		this.armas = [];

		//rato
		this.mouseD = false;

		//som
		this.som = null;

		//limite do cenario
		this.limite = limite;
	}

	//funçoes de desanho e de clear são executadas pelo pai
	draw(ctx){
		if(this.keyR==true){
			this.dir = 1;
			this.num_index +=1;
			if(this.num_index>10){
				this.num_index = 0;
				this.index +=1;
				if(this.index > 4){
					this.index = 0;
				}
			}
			
		}
		if(this.keyL==true){
			this.dir = -1;
			this.num_index +=1;
			if(this.num_index>10){
				this.num_index = 0;
				this.index +=1;
				if(this.index > 9){
					this.index = 5;
				}
			}
		}
		if(this.keyR == false && this.keyL == false)
		{
			if(this.dir==1){
				this.index = 0;
			}
			else{
				this.index = 5;
			}
		}
		//ctx.putImageData(this.img[Math.floor(this.index/10)], this.x-this.xrelative, this.y);
		ctx.drawImage(this.img[this.index], this.x, this.y);

	}
	update(ctx, inimigos,NIVEL, collectibles){
		if(this.keyU==false && this.teste_salto == false){
			this.teste_salto = true;
			this.salto = 0;
		}

		if (this.keyU ==true && this.teste_salto==false){
			this.teste_salto = true;
		}
		if(this.teste_salto){
			if(this.salto<=0){
				this.y +=this.gravidade;
				this.gravidade+=0.3;
				if(this.gravidade>10){
					this.gravidade = 10;
				}
			}else{
				this.y -= this.salto/2;
				
				this.salto-= 1/2;
				if(this.keyU==false){
					this.salto-= 1;
				}
			}
			
		}
		if (this.keyD==true){
			this.y += this.speed;
			//verifica se pode andar para baixo
			for(let i=1; i<inimigos.length;i++)
			{
				if(this.colided(inimigos[i])){
					this.keyD = false;
					this.y-=this.speed;
					break;
				}
			}
			
			if(this.y>600-this.height){
				this.y = 600-this.height;
			}
		}
		if (this.keyL==true){
			this.x -= this.speed;
			// nao faz verificacao para nao ficar "preso"
			/*for(let i=1; i<inimigos.length;i++)
			{
				if(this.colided(inimigos[i])){
					this.keyL = false;
					this.x+=this.speed;
					break;
				}
			}*/
			if(this.x<0){
				this.x = 0;
			}

		}
		if (this.keyR==true){
			// calculo do movimento

			this.x += this.speed;
			//verifica se pode avancar
			for(let i=1; i<inimigos.length;i++)
			{
				if(this.colided(inimigos[i])){
					this.keyR = false;
					this.x-=this.speed;
					break;
				}
			}
			if(this.x>650-this.width){
				this.x =650-this.width;
			}
		}
		// regeneracao vida
		if(this.health<this.healthIni){
			if (0.1-(NIVEL*0.01)>0)
				this.health+= 0.05-(NIVEL*0.01);
		}
		
		//collectibles
		for(let i=0; i<collectibles.length;i++)	{
			if (collectibles[i].img.id == "collectibles"){
				if(this.BoundingBox(collectibles[i]))
				{
					this.collected+=1
					collectibles.splice(i, 1);
				}
			}
		}

		if (this.mouseD ==true)
		{
			this.armas.push(this.armas[0]);//copia para o fim a arma[0]
			this.armas[this.armas.length-1].usable = true;// e define como usavel
		}
		var total = this.armas.length;
		if(total>1)
		{
			for(let i=0; i<this.armas.length;i++)
			{
				if(this.armas[i].usable)
				{
					this.armas[i].x = this.x+this.width-10;
					this.armas[i].y = this.y+this.height/3;
					this.armas[i].usable = false;
				}
				else
				{
					this.armas[i].x = this.armas[i].x + this.armas[i].speed;
					if(this.armas[i].x>1000+this.armas[i].speed && this.armas.length>1)
						this.armas.splice(i,1);
				}
				if(this.armas.length == 1){
					this.armas[0].usable = true;
				}
			}
		}

		/*
		******************************************************************************
							DETETAR SE ARMA ATINGE INIMIGO
		******************************************************************************
		*/
		if(total>1)
		{
			var armasToRemove=[];
			for(let i=0; i<this.armas.length;i++)
			{
				for(let j = 1; j<inimigos.length;j++){
					if (this.armas[i].BoundingBox(inimigos[j]) && this.armas[i].usable==false)
					{
						this.armas[i].usable = true;// para nao causar mais danos a nenhum inimigo
						this.armas[i].damagePersonagem(inimigos[j]);
						if(inimigos[j].health<=0){
							inimigos.splice(j,1);
						}
						armasToRemove.push(i);//guarda arma a remover
					}
				}
			}
			//remove armas 
			for(let i=0; i<armasToRemove.length;i++)
				this.armas.splice(armasToRemove[i],1);
		}
			

		//actualiza xrelative

		if(this.x>500 && this.xrelative<this.limite){
			var perc = (this.x-500)/150;
			if(perc>1){perc==1;}
			if(perc<0){perc==0;}

			this.x-=this.speed*perc;
			this.xrelative+=this.speed*perc;

		}
		if(this.x<200 && this.xrelative>0){

			var perc = 1-((this.x)/350);
			if(perc>1){perc==1;}
			if(perc<0){perc==0;}

			this.x+=this.speed*perc;
			this.xrelative-=this.speed*perc;
		}

		if(this.xrelative<0){this.xrelative=0;}
		if(this.xrelative>this.limite){this.xrelative=this.limite;}
		return this.xrelative;

	}
	criaImg(img){

		var c = document.createElement("canvas");
		var ctx2 = c.getContext("2d");
		ctx2.drawImage(img,0,0);

		this.imgData.push(ctx2.getImageData(0,0,60,60));
		this.imgData.push(ctx2.getImageData(60,0,60,60));
		this.imgData.push(ctx2.getImageData(120,0,60,60));
		this.imgData.push(ctx2.getImageData(180,0,60,60));
		this.imgData.push(ctx2.getImageData(240,0,60,60));

		this.imgData.push(ctx2.getImageData(240,60,60,60));
		this.imgData.push(ctx2.getImageData(180,60,60,60));
		this.imgData.push(ctx2.getImageData(120,60,60,60));
		this.imgData.push(ctx2.getImageData(60,60,60,60));
		this.imgData.push(ctx2.getImageData(0,60,60,60));

		for (let i = 0; i<this.imgData.length;i++){
			this.img.push(this.imagedata_to_image(this.imgData[i]))
		}

		console.log(this.img.length);
	}

	imagedata_to_image(imagedata) {

		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
		canvas.width = imagedata.width;
		canvas.height = imagedata.height;
		ctx.putImageData(imagedata, 0, 0);

		var image = new Image();
		image.src = canvas.toDataURL();
		return image;
	}
	bloco(spimage){
		if(this.y+this.height<spimage.y+spimage.height){
			this.y = spimage.y-this.height;
			this.teste_salto = false;
			this.salto = 20;
		}
		else if(this.y>spimage.y){
			this.y = spimage.y+spimage.height;
			this.teste_salto = true;
			this.salto = 0;
		}
	}
}