"use strict";

class menu_lose{

	constructor(canvas,ctx){
		this.canvas = canvas;
		this.ctx = ctx;
		this.spArray;
	}

	carrega(){
		this.init(this.ctx);	
	}

	init(ctx){
		var spArray = [];

        var texto_win = new Texto(500,250,"Você Perdeu!","#FFFFFF",80);
        spArray.push(texto_win);
        var botao = new Botao_menu(375,350,100,30,"rgba(120,120,120,","Repetir");
        spArray.push(botao);
        var botao = new Botao_menu(575,350,100,30,"rgba(120,120,120,","Menu");
        spArray.push(botao);

        var ev2 = new Event("comecar");
        ev2.spArray = spArray;
        ctx.canvas.dispatchEvent(ev2);
		
	}

	draw()
	{
        this.ctx.fillStyle = "rgba(100,100,100,0.01)";
		this.ctx.fillRect(100,100,800,400);
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			this.spArray[i].draw(this.ctx);
		}
	}

	//funcao responsavel por actualizar as posicoes
	render()
	{
		this.draw();
	}
	mouseMove(posX,posY){
		var teste = false;
		var dim = this.spArray.length;

		for (let i = 0; i < dim; i++){
			if(this.spArray[i].dentro(posX,posY)!=""){
				teste = true;
			}
		}

		if (teste){
			document.getElementById("canvas").style.cursor = "pointer";
		}else{
			document.getElementById("canvas").style.cursor = "initial";
		}
	}

	mouseClick(posX,posY){
		var botao = "";
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			botao = this.spArray[i].dentro(posX,posY);
			if(botao != ""){
				break;
			}
		}
		if(botao=="Repetir"){
			var ev3 = new Event("lose_repetir");
			this.ctx.canvas.dispatchEvent(ev3);
		}
        if(botao=="Menu"){
			var ev3 = new Event("lose_menu");
			this.ctx.canvas.dispatchEvent(ev3);
		}
	}

}