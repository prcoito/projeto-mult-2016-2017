"use strict";

var som = 3;//0-5
var musica = 3;//0-5
var dificuldade = 2;//0-5
var fase = 10;
var nivel = 1;

(function()
{
	window.addEventListener("load", main);
}());

function main()
{
    var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	menuBase(ctx,canvas);
}

function menuBase(ctx,canvas){

	var estado = 1;

	var menu = new menu_inicial(canvas,ctx);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();
			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("menu_sair",menuSairHandler);
			canvas.addEventListener("menu_creditos",menuCreditosHandler);
			canvas.addEventListener("menu_opcoes",menuOpcoesHandler);
			canvas.addEventListener("menu_jogar",menuJogarHandler);
	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function menuSairHandler(ev){
		removeEventos();
		estado = 0;
		window.close();
	}
	function menuCreditosHandler(ev){
		removeEventos();
		estado = 0;
		menuCreditos(ctx,canvas);
	}
	function menuOpcoesHandler(ev){
		removeEventos();
		estado = 0;
		menuOpcoes(ctx,canvas);
	}
	function menuJogarHandler(ev){
		removeEventos();
		estado = 0;
		menuJogar(ctx,canvas);
	}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("menu_sair",menuSairHandler);
		canvas.removeEventListener("menu_creditos",menuCreditosHandler);
		canvas.removeEventListener("menu_opcoes",menuOpcoesHandler);
		canvas.removeEventListener("menu_jogar",menuJogarHandler);
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}

function menuCreditos(ctx,canvas){
	var estado = 1;

	var menu = new menu_creditos(canvas,ctx);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();
			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("creditos_sair",creditosSairHandler);
	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function creditosSairHandler(ev){
		removeEventos();
		estado = 0;
		menuBase(ctx,canvas);
	}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("creditos_sair",creditosSairHandler);
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}

function menuOpcoes(ctx,canvas){
	var estado = 1;

	var menu = new menu_opcoes(canvas,ctx,som,musica,dificuldade);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();

			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("creditos_sair",creditosSairHandler);


			canvas.addEventListener("som_mais",somMaisHandler);
			canvas.addEventListener("som_menos",somMenosHandler);

			canvas.addEventListener("musica_mais",musicaMaisHandler);
			canvas.addEventListener("musica_menos",musicaMenosHandler);

			canvas.addEventListener("dificuldade_mais",dificuldadeMaisHandler);
			canvas.addEventListener("dificuldade_menos",dificuldadeMenosHandler);

	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function creditosSairHandler(ev){
		removeEventos();
		estado = 0;
		menuBase(ctx,canvas);
	}
	function somMaisHandler(ev){menu.adiciona(0);som+=1;if(som>5){som=5;}}
	function somMenosHandler(ev){menu.subtrai(0);som-=1;if(som<0){som=0;}}

	function musicaMaisHandler(ev){menu.adiciona(1);musica+=1;if(musica>5){musica=5;}}
	function musicaMenosHandler(ev){menu.subtrai(1);musica-=1;if(musica<0){musica=0;}}

	function dificuldadeMaisHandler(ev){menu.adiciona(2);dificuldade+=1;if(dificuldade>5){dificuldade=5;}}
	function dificuldadeMenosHandler(ev){menu.subtrai(2);dificuldade-=1;if(dificuldade<0){dificuldade=0;}}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("creditos_sair",creditosSairHandler);

		canvas.removeEventListener("som_mais",somMaisHandler);
		canvas.removeEventListener("som_menos",somMenosHandler);

		canvas.removeEventListener("musica_mais",musicaMaisHandler);
		canvas.removeEventListener("musica_menos",musicaMenosHandler);

		canvas.removeEventListener("dificuldade_mais",dificuldadeMaisHandler);
		canvas.removeEventListener("dificuldade_menos",dificuldadeMenosHandler);
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}


function menuJogar(ctx,canvas){
	var estado = 1;

	var menu = new menu_jogar(canvas,ctx,fase);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();

			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("creditos_sair",creditosSairHandler);
			canvas.addEventListener("nivel",nivelComecarHandler)

	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function creditosSairHandler(ev){
		removeEventos();
		estado = 0;
		menuBase(ctx,canvas);
	}
	function nivelComecarHandler(ev){
		removeEventos();
		estado = 0;
		nivel = ev.nivel;
		jogo(ctx,canvas,ev.nivel);
	}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("creditos_sair",creditosSairHandler);
		canvas.removeEventListener("nivel",nivelComecarHandler)
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}

function jogo (ctx,canvas,nivel){
	var estado = 1;
	var pause = false;

	var menu = new Jogo(canvas,ctx,nivel,som,musica,dificuldade);

    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    

	function comecarHandler(ev)
		{
			canvas.removeEventListener("comecar",comecarHandler);
			//canvas.removeEventListener("comecar",comecarHandler);
			window.addEventListener("keydown", kdh);
			window.addEventListener("keyup", kuh);
			ctx.canvas.addEventListener("mousedown", mouseD);
			ctx.canvas.addEventListener("mouseup", mouseU);

			canvas.addEventListener("morreu", morreuHandler)
			canvas.addEventListener("venceu",venceuHandler);

			canvas.addEventListener("pause",pauseHandler);

			
			menu.spArray = ev.spArray;
			menu.pArray = ev.pArray;
			//menu.armas = ev.armas;
			//menu.armasI = ev.armasI;
			menu.arrayUI = ev.arrayUI;

			menu.carregaBlocos();

			menu.startAnime();
			animLoop();
			
		}
		function pauseHandler(ev){
			if( estado == 0){
				estado = 1;
				animLoop();

				window.removeEventListener("keydown", pauseHandler);

				window.addEventListener("keydown", kdh);
				window.addEventListener("keyup", kuh);
				ctx.canvas.addEventListener("mousedown", mouseD);
				ctx.canvas.addEventListener("mouseup", mouseU);

				canvas.addEventListener("morreu", morreuHandler)
				canvas.addEventListener("venceu",venceuHandler);

				canvas.addEventListener("pause",pauseHandler);

				pause = false;

			}else{

				estado = 0;
				removeEventos();
				pause = true;

				window.addEventListener("keydown", pauseHandler);
			}
			
		}

		var venceuHandler = function(ev){
			console.log("VENCEU");
			estado = 0;
			removeEventos();
			menuWin(ctx,canvas);

		}

		var morreuHandler = function(ev){
			estado = 0;
			removeEventos();
			menuLose(ctx,canvas);
		}
		var kdh = function(ev){
			menu.keyDownHandler(ev, menu.pArray[0]);
			//pArray[0] = keyDownHandler(ev, pArray[0]);
		}
		var kuh = function(ev){
			menu.keyUpHandler(ev, menu.pArray[0]);
			//pArray[0] = keyUpHandler(ev, pArray[0]);
		}
		var mouseU = function(ev){
			menu.mouseUpHandler(ev, menu.pArray[0]);
			//pArray[0] = mouseUpHandler(ev, pArray[0]);
		}
		var mouseD = function(ev){
			menu.mouseDownHandler(ev, menu.pArray[0]);
			///pArray[0] = mouseDownHandler(ev, pArray[0]);
		}

	function removeEventos(){
		window.removeEventListener("keydown", kdh);
		window.removeEventListener("keyup", kuh);
		ctx.canvas.removeEventListener("mousedown", mouseD);
		ctx.canvas.removeEventListener("mouseup", mouseU);

		canvas.removeEventListener("morreu", morreuHandler)
		canvas.removeEventListener("venceu",venceuHandler);

		canvas.removeEventListener("pause",pauseHandler);
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
			menu.render();
		}else{
			if(pause){
				ctx.fillStyle = ("rgba(100,100,100,0.5");
				ctx.fillRect(100,100,800,400);
				ctx.fillStyle = "#FFFFFF";
				ctx.font = "100px verdana";
				ctx.textAlign = 'center';
				ctx.fillText("PAUSE", 500, 330);
			}
		}
		
	}
}


function menuWin(ctx,canvas){
	var estado = 1;

	var menu = new menu_win(canvas,ctx);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();
			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("win_continuar",winContinuarHandler);
			canvas.addEventListener("win_sair",winSairHandler);
	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function winSairHandler(ev){
		estado = 0;
		removeEventos();
		if(nivel == fase && fase!=10){
			fase++;
		}
		menuBase(ctx,canvas);
	}

	function winContinuarHandler(ev){
		estado = 0;
		removeEventos();
		if(nivel == fase && fase!=10){
			fase++;
		}

		nivel++;
		if(nivel>10){
			nivel=10;
		}
		jogo(ctx,canvas,nivel);

	}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("win_continuar",winContinuarHandler);
		canvas.removeEventListener("win_sair",winSairHandler);
		
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}


function menuLose(ctx,canvas){
	var estado = 1;

	var menu = new menu_lose(canvas,ctx);
    canvas.addEventListener("comecar",comecarHandler);
    menu.carrega();
    
    function comecarHandler(ev){
			canvas.removeEventListener("comecar",comecarHandler);
			menu.spArray = ev.spArray;
			menu.draw();
			animLoop();
			canvas.addEventListener("mousemove",mouseMoveHandler);
			canvas.addEventListener("click",mouseClickHandler);
			canvas.addEventListener("lose_repetir",loseRepetirHandler);
			canvas.addEventListener("lose_menu",loseMenuHandler);
	}
    function mouseMoveHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseMove(posX,posY);
	}
    function mouseClickHandler(ev){
			var rect = canvas.getBoundingClientRect();
			var posX = ev.clientX- rect.left;
			var posY = ev.clientY- rect.top;
			menu.mouseClick(posX,posY);
	}
	function loseRepetirHandler(ev){
		estado = 0;
		removeEventos();
		jogo(ctx,canvas,nivel);
		
	}

	function loseMenuHandler(ev){
		estado = 0;
		removeEventos();
		menuBase(ctx,canvas);

	}

	function removeEventos(){
		canvas.removeEventListener("mousemove",mouseMoveHandler);
		canvas.removeEventListener("click",mouseClickHandler);
		canvas.removeEventListener("lose_repetir",loseRepetirHandler);
		canvas.removeEventListener("lose_menu",loseMenuHandler);
		
	}

	function animLoop()
	{	
		var al = function(time)
		{
			animLoop();
		}
		if(estado==1){
			var reqID = window.requestAnimationFrame(al);
		}
		menu.render();
	}
}