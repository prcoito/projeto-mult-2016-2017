"use strict";

class menu_jogar{

	constructor(canvas,ctx,fase){
		this.canvas = canvas;
		this.ctx = ctx;
		this.spArray;
		this.fase = fase;
	}

	carrega(){
		this.init(this.ctx);	
	}

	init(ctx){
		var spArray = [];
		var img = new Image();
		var fase = this.fase;

		img.addEventListener("load",imgLoadHandler);
		img.id = "fundo";
		img.src = "imagens/fundo_creditos.png";

		function imgLoadHandler(ev){
			ev.target.removeEventListener("load",imgLoadHandler);
			var cenario = new SpriteImage_menu(0,0,1000,600,ev.target);
			spArray.push(cenario);

			var botao = new Botao_menu(950,20,30,30,"rgba(139,69,19,","X");
			spArray.push(botao);


			var btn_fase1 = new botao_fase(250,75,"rgba(139,69,20,",1,fase>=1);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(450,75,"rgba(139,69,20,",2,fase>=2);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(650,75,"rgba(139,69,20,",3,fase>=3);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(250,200,"rgba(139,69,20,",4,fase>=4);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(450,200,"rgba(139,69,20,",5,fase>=5);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(650,200,"rgba(139,69,20,",6,fase>=6);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(250,325,"rgba(139,69,20,",7,fase>=7);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(450,325,"rgba(139,69,20,",8,fase>=8);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(650,325,"rgba(139,69,20,",9,fase>=9);
			spArray.push(btn_fase1);
			var btn_fase1 = new botao_fase(450,450,"rgba(139,69,20,",10,fase>=10);
			spArray.push(btn_fase1);
			

			var ev2 = new Event("comecar");
			ev2.spArray = spArray;
			ctx.canvas.dispatchEvent(ev2);
		}
		
	}

	draw()
	{
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			this.spArray[i].draw(this.ctx);
		}
	}

	//funcao responsavel por actualizar as posicoes
	render()
	{
		this.draw();
	}
	mouseMove(posX,posY){
		var teste = false;
		var dim = this.spArray.length;

		for (let i = 0; i < dim; i++){
			if(this.spArray[i].dentro(posX,posY)!=""){
				teste = true;
			}
		}

		if (teste){
			document.getElementById("canvas").style.cursor = "pointer";
		}else{
			document.getElementById("canvas").style.cursor = "initial";
		}
	}

	mouseClick(posX,posY){
		var botao = "";
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			botao = this.spArray[i].dentro(posX,posY);
			if(botao != ""){
				break;
			}
		}
		if(botao=="X"){
			var ev3 = new Event("creditos_sair");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "1_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 1;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "2_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 2;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "3_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 3;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "4_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 4;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "5_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 5;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "6_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 6;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "7_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 7;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "8_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 8;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "9_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 9;
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "10_btn"){
			var ev3 = new Event("nivel");
			ev3.nivel = 10;
			this.ctx.canvas.dispatchEvent(ev3);
		}

	}

}
	