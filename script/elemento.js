"use strict";
//objectos no cenario que tem função ou movemento mas que não tem IA
// por exempo, bandeira de inicio ou de fim
// plataforma que se move

class Elemento extends SpriteImage
{
	constructor (x,y,w,h,tipo,img)
	{
		super(x,y,w,h,img);

		this.tipo = tipo;

		this.estado = 0;
		this.Aux1 = 0;
		this.Aux2 = 0;
	}

	//funçoes de desanho e de clear são executadas pelo pai
	draw(ctx){super.draw(ctx);}
	clear(ctx){super.clear(ctx);}
	update(ctx){

	}
}