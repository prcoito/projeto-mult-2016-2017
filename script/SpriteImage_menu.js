"use strict";

class SpriteImage_menu
{
	constructor(x,y,w,h,img)
	{
		this.largura = 1000;
		
		this.x = x;
		this.y = y;

		this.xrelative = 0;

		this.width = w;
		this.height = h;

		this.img = img;

		//teclas
		this.keyU = false;
		this.keyD = false;
		this.keyL = false;
		this.jeyR = false;
	}
	draw(ctx)
	{
		ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
	}
	colocaImage(ctx){
		ctx.putImageData(this.img,this.x,this.y);
	}
	clear(ctx)
	{
		ctx.clearRect(this.x, this.y, this.width, this.height);
	}
	update(ctx){

	}
	/*
		DETETAR COLISOES
	*/
	colided(sprite){
		if(this.BoundingBox(sprite))
		{
			var nlinhas  = (sprite.y + sprite.height) - this.y;
			var nColunas = (this.x + this.width) - sprite.x;
			
			for(let i=0;i<nlinhas; i++){
				var posT = Math.round(i);
				var posC = Math.round((this.width)*(sprite.y-this.y+i) + (sprite.x-this.x));
				for (let j = 0; j<nColunas;j++){
					if (this.alpha[posC+j]==255 && sprite.alpha[posT+j]==255)
					{
						return true;
					}
				}
			}
			return false;
		}
		else
			return false;
	}
	BoundingBox(sprite){
		if (this.x < sprite.x + sprite.width && this.x + this.width > sprite.x &&
			this.y < sprite.y + sprite.height && this.y + this.height > sprite.y)
			return true;
		else
			return false;
	}
	/*
		FIM DETECAO COLISOES
	*/
	dentro(posX,posY){
		return false;
	}
}