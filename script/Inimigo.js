"use strict";

class Inimigo extends SpriteImage
{
	constructor (x,y,w,h,tipo,img, health, speed, alpha)
	{
		/*if(img.id == "PPrincipal")
			super(x,y,w/5,h,img);//  w/5 porque sprite tem 5 imagens
		else
			*/
        super(x,y,w,h,img);

		this.tipo = tipo;
		this.speed = speed;
		this.alpha = alpha;
		
		this.estado = 0;
		this.Aux1 = 0;
		this.Aux2 = 0;

		this.health = health;
		this.index = 0;

		this.armas = [];

		//som
		this.som = null;
	}

	//funçoes de desanho e de clear são executadas pelo pai
	draw(ctx){
		/*if (this.img.id == "PPrincipal"){
			if(this.keyR==true ||this.keyL==true){
				this.index = (this.index+1)%40 + 1;
			}
			if(this.keyU ==false && this.keyD ==false && this.keyL ==false && this.keyR ==false)
				this.index = 0;

			ctx.drawImage(this.img, this.width * Math.floor(this.index/10), 0, this.width, this.height, this.x, this.y, this.width, this.height);
		}
		else
			*/
        super.draw(ctx);
	}
	clear(ctx){super.clear(ctx);}
    /* ************************************************************************************************* 
	 *                                IA
	 * *************************************************************************************************
	*/
	update(ctx, personagem, NIVEL){
		/*if (personagem.keyU ==true && personagem.y>400){
			if (Math.random()<(1-NIVEL*0.25))
			{
				this.y += this.speed;
				if(this.y>500){
					this.y = 500;
				}
			}

		}
		if (personagem.keyD==true && personagem.y<500){
			if (Math.random()<(1-NIVEL*0.25))
			{
				this.y -= this.speed;
				if(this.y<400){
					this.y = 400;
				}
			}
		}*/
		this.x = this.xini - this.xrelative;
		
		/*if(personagem.keyU ==false && personagem.keyD ==false && personagem.keyL ==false && personagem.keyR ==false){
			if(this.y>personagem.y+this.speed){
				if (Math.random()<(1-NIVEL*0.25))
				{
					this.y -= this.speed;
					if(this.y<400){
						this.y = 400;
					}
				}
			}
			else if(this.y<personagem.y-this.speed){
				if (Math.random()<(1-NIVEL*0.25))
				{	
					this.y += this.speed;
					if(this.y>500){
						this.y = 500;
					}
				}
			}
		}*/
		if( (this.x-personagem.x<500) &&(this.x-personagem.x>0) && (Math.random()<0.005*NIVEL) ){
			this.armas.push(this.armas[0]);//copia para o fim a arma[0]
			this.armas[this.armas.length-1].usable = true;// e define como usavel
		}
		var total = this.armas.length;
		if(total>1)
		{
			for(let i=0; i<this.armas.length;i++)
			{
				if(this.armas[i].usable)
				{
					this.armas[i].x = this.x-this.width-10;
					this.armas[i].y = this.y+this.height/3;
					this.armas[i].usable = false;
				}
				else
				{
					this.armas[i].x = this.armas[i].x - this.armas[i].speed;
					if(this.armas[i].x<0 && this.armas.length>1)
						this.armas.splice(i,1);
				}
				if(this.armas.length == 1){
					this.armas[0].usable = true;
				}
			}
		}

		/*
		******************************************************************************
							DETETAR SE ARMA ATINGE Personagem
		******************************************************************************
		*/
		
		if(total>1)
		{
			var armasToRemove=[];
			for(let i=0; i<this.armas.length;i++)
			{
				if (this.armas[i].BoundingBox(personagem) && this.armas[i].usable==false)//BoundingBox para ser mais dificil de jogar
				{
					this.armas[i].usable = true;// para nao causar mais danos a nenhum inimigo
					this.armas[i].damagePersonagem(personagem);
					if(personagem.som!=null)
						personagem.som.play();
					armasToRemove.push(i);//guarda arma a remover
				}
			}
			//remove armasI 
			for(let i=0; i<armasToRemove.length;i++)
				this.armas.splice(armasToRemove[i],1);
		}
		
	}
}