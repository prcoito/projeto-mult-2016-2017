"use strict";
// onde estao as varias camadas do cenario
class Cenario extends SpriteImage
{

	constructor (x,y,w,h,tipo,img)
	{
		super(x,y,w,h,img);

		this.tipo = tipo;

	}

	//funçoes de desanho e de clear são executadas pelo pai
	draw(ctx){super.draw(ctx);}
	clear(ctx){super.clear(ctx);}
	update(ctx){
		if(this.tipo == 2 ){
			this.x = -this.xrelative/4;
		}
		if(this.tipo == 3 ){
			this.x = -this.xrelative/2;
		}
		if(this.tipo == 4 ){
			this.x = -this.xrelative;
		}
	}
	BoundingBox(sprite){
		return false;
	}
}