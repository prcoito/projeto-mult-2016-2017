"use strict";

class menu_inicial{

	constructor(canvas,ctx){
		this.canvas = canvas;
		this.ctx = ctx;
		this.spArray;
	}

	carrega(){
		this.init(this.ctx);	
	}

	init(ctx){
		var spArray = [];
		var img = new Image();
		img.addEventListener("load",imgLoadHandler);
		img.id = "fundo";
		img.src = "imagens/fundo_menu.png";

		function imgLoadHandler(ev){
			ev.target.removeEventListener("load",imgLoadHandler);
			var cenario = new SpriteImage_menu(0,0,1000,600,ev.target);
			spArray.push(cenario);

			var titulo = new Image();
			titulo.addEventListener("load",tituloLoadHandler);
			titulo.id = "titulo";
			titulo.src= "imagens/titulo_menu.png";
		}

		function tituloLoadHandler(ev){
			ev.target.removeEventListener("load",tituloLoadHandler);
			var cenario = new SpriteImage_menu(0,0,1000,600,ev.target);
			spArray.push(cenario);

			var botao = new Botao_menu(400,350,200,30,"rgba(139,69,19,","Jogar");
			spArray.push(botao);
			var botao = new Botao_menu(400,400,200,30,"rgba(139,69,19,","Opções");
			spArray.push(botao);
			var botao = new Botao_menu(400,450,200,30,"rgba(139,69,19,","Créditos");
			spArray.push(botao);
			var botao = new Botao_menu(400,500,200,30,"rgba(139,69,19,","Sair");
			spArray.push(botao);

			var ev2 = new Event("comecar");
			ev2.spArray = spArray;
			ctx.canvas.dispatchEvent(ev2);
		}
		
	}

	draw()
	{
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			this.spArray[i].draw(this.ctx);
		}
	}

	//funcao responsavel por actualizar as posicoes
	render()
	{
		this.draw();
	}
	mouseMove(posX,posY){
		var teste = false;
		var dim = this.spArray.length;

		for (let i = 0; i < dim; i++){
			if(this.spArray[i].dentro(posX,posY)!=""){
				teste = true;
			}
		}

		if (teste){
			document.getElementById("canvas").style.cursor = "pointer";
		}else{
			document.getElementById("canvas").style.cursor = "initial";
		}
	}

	mouseClick(posX,posY){
		var botao = "";
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			botao = this.spArray[i].dentro(posX,posY);
			if(botao != ""){
				break;
			}
		}
		if(botao=="Sair"){
			var ev3 = new Event("menu_sair");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao=="Créditos"){
			var ev3 = new Event("menu_creditos");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao=="Opções"){
			var ev3 = new Event("menu_opcoes");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao=="Jogar"){
			var ev3 = new Event("menu_jogar");
			this.ctx.canvas.dispatchEvent(ev3);
		}
	}

}
	