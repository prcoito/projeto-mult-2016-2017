"use strict";
//blocos estáticos do jogo
class Flag extends SpriteImage
{
	constructor (x,y,w,h,img)
	{
		super(x,y,w,h,img);
        this.personagem = null;
	}

	draw(ctx){
		ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
	}
	clear(ctx){super.clear(ctx);}
	update(ctx){
		this.x = this.xini-this.xrelative;
		if(this.BoundingBox(this.personagem)){
			var event = new Event("venceu");
			ctx.canvas.dispatchEvent(event);
		}
	}
}