"use strict";
// onde estao as varias camadas do cenario
class Botao_menu extends SpriteImage_menu
{

	constructor (x,y,w,h,cor,texto)
	{
		super(x,y,w,h,null);
		this.cor = cor;
		this.texto = texto;
		this.nome = texto;
		this.estado = 0;

	}
	nome(nome){
		this.nome = nome;
	}
	
	draw(ctx){
		if(this.estado == 0){
			ctx.fillStyle = this.cor+"0.5)";
			ctx.fillRect(this.x,this.y,this.width,this.height);
			ctx.fillStyle = "#EEEEEE"
			ctx.font = '20px verdana';
			ctx.textAlign = 'center';
			ctx.fillText(this.texto, this.x+this.width/2, this.y+22);
		}
		if(this.estado == 1){

			ctx.fillStyle = this.cor+"0.9)";
			ctx.fillRect(this.x,this.y,this.width,this.height);
			ctx.fillStyle = "#CCCCCC"
			ctx.font = '20px verdana';
			ctx.textAlign = 'center';
			ctx.fillText(this.texto, this.x+this.width/2, this.y+22);
		}
	}
	dentro(posX,posY){
		if (this.x < posX  && this.x + this.width > posX &&
			this.y < posY && this.y + this.height > posY){
				this.estado = 1;			
				return this.nome;
			}
		else{
			this.estado = 0;
			return "";
		}	
	}
}