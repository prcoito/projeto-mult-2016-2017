"use strict";

class Collectible extends SpriteImage
{
	constructor(x,y,w,h,img)
	{
        super(x,y,w/8,h, img);
        
		this.index = 0;
    }
        
    draw(ctx)
	{  
        this.x = this.xini - this.xrelative;
        this.index = (this.index+1)%40;
        ctx.drawImage(this.img, this.width * Math.floor(this.index/5), 0, this.width, this.height, this.x, this.y, this.width, this.height);
    }
}
			