"use strict";


class Nivel
{
	constructor(num){
        this.nivel = num;

        this.posInimigos = [];
        this.posInimigos.push(0);
        
        this.numInimigos = [];
        
        this.cenario = [];
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");
        this.cenario.push("fundo_01.png;fundo_02.png;fundo_03.png;fundo_04.png");

        this.cenarioNivel = this.cenario[nivel-1];
        
        this.blocos = [];
        this.blocos.push("-100,500,100,4,#964b00;-100,470,100,1,#00AA00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;800,500,100,4,#964b00;800,470,100,1,#00AA00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;1200,500,100,4,#964b00;1200,470,100,1,#00AA00;700,320,7,1,#FFFF00");
        this.blocos.push("-100,500,100,4,#964b00;-100,470,100,1,#00AA00;600,300,5,1,#FFFF00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;700,500,20,4,#964b00;700,470,20,1,#00AA00;1500,500,50,4,#964b00;1500,470,50,1,#00AA00;");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;900,500,20,4,#964b00;900,470,20,1,#00AA00;1900,500,50,4,#964b00;1900,470,50,1,#00AA00;600,300,6,1,#FFFF00;1500,300,6,1,#FFFF00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;2300,500,50,4,#964b00;2300,470,50,1,#00AA00;800,300,6,1,#FFFF00;1200,300,6,1,#FFFF00;1700,300,6,1,#FFFF00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;2300,500,50,4,#964b00;2300,470,50,1,#00AA00;800,300,6,1,#FFFF00;800,500,6,1,#FFFF00;1200,150,6,1,#FFFF00;1200,500,6,1,#FFFF00;1700,300,6,1,#FFFF00");
        this.blocos.push("-100,500,20,4,#964b00;-100,470,20,1,#00AA00;700,400,20,10,#964b00;700,370,20,1,#00AA00;1500,500,20,10,#964b00;1500,470,20,1,#00AA00;2500,500,50,10,#964b00;2500,470,50,1,#00AA00;2200,300,6,1,#FFFF00");
        this.blocos.push("100,500,6,1,#FFFFFF;400,400,4,1,#FFFFFF;700,200,4,1,#FFFFFF;1100,500,4,1,#FFFFFF;1500,500,4,1,#FFFFFF;1800,400,4,1,#FFFFFF;2100,200,4,1,#FFFFFF;2500,470,10,1,#FFFFFF;");

        this.blocosNivel = this.blocos[nivel-1];


        this.collectibles = [];

        this.collectibles.push("1000,300;800,300;500,300");
        this.collectibles.push("1000,300;800,300;500,300");
        this.collectibles.push("1000,180;800,260;500,300");
        this.collectibles.push("1000,300;800,300;500,300");
        this.collectibles.push("1600,300;1000,300;800,300");
        this.collectibles.push("1600,200;1000,150;800,200");
        this.collectibles.push("1600,200;1000,150;800,200");
        this.collectibles.push("1500,350;1000,150;800,200");
         this.collectibles.push("800,300;1600,350;2300,200");
        this.collectibles.push("750,100;2150,100;2300,200");


        this.collectiblesNivel = this.collectibles[nivel-1];
        this.numCollectibles = 3;

        this.fim = []
        this.fim.push(1200);
        this.fim.push(1200);
        this.fim.push(1400);
        this.fim.push(1200);
        this.fim.push(1200);
        this.fim.push(2300);
        this.fim.push(2700);
        this.fim.push(2700);
        this.fim.push(2800);
        this.fim.push(2900);

        this.fim_val = this.fim[nivel-1];


        this.flag_pos = [];
        this.flag_pos.push([1100,270]);
        this.flag_pos.push([1000,270]);
        this.flag_pos.push([1400,270]);
        this.flag_pos.push([1000,270]);
        this.flag_pos.push([1800,270]);
        this.flag_pos.push([2200,270]);
        this.flag_pos.push([2600,270]);
        this.flag_pos.push([2600,270]);
        this.flag_pos.push([2700,270]);
        this.flag_pos.push([2700,270]);

        this.flag = this.flag_pos[nivel-1];

        this.num_inimigos = [];
        this.num_inimigos.push("");
        this.num_inimigos.push("");
        this.num_inimigos.push("");
        this.num_inimigos.push("800,490;1800,490");
        this.num_inimigos.push("900,490;1600,490");
        this.num_inimigos.push("900,490;1200,490");
        this.num_inimigos.push("");
        this.num_inimigos.push("");
        this.num_inimigos.push("");
        this.num_inimigos.push("");

        this.num_nivel_inimigos = this.num_inimigos[nivel-1];
    }
}