"use strict";

class menu_opcoes{

	constructor(canvas,ctx,som,musica,dificuldade){
		this.canvas = canvas;
		this.ctx = ctx;
		this.spArray;
		this.som = som;
		this.musica = musica;
		this.dificuldade = dificuldade;
	}

	carrega(){
		this.init(this.ctx);	
	}

	init(ctx){
		var spArray = [];
		var img = new Image();
		var som = this.som;
		var musica = this.musica;
		var dificuldade = this.dificuldade;

		img.addEventListener("load",imgLoadHandler);
		img.id = "fundo";
		img.src = "imagens/fundo_creditos.png";

		function imgLoadHandler(ev){
			ev.target.removeEventListener("load",imgLoadHandler);
			var cenario = new SpriteImage_menu(0,0,1000,600,ev.target);
			spArray.push(cenario);

			var botao = new Botao_menu(950,20,30,30,"rgba(139,69,19,","X");
			spArray.push(botao);

			//som
			var botao_som_mais = new Botao_menu(605,200,30,30,"rgba(139,69,19,","+");
			botao_som_mais.nome = "+som";
			spArray.push(botao_som_mais);
			var botao_som_menos = new Botao_menu(365,200,30,30,"rgba(139,69,19,","-");
			botao_som_menos.nome = "-som";
			spArray.push(botao_som_menos);
			var barra_som = new nivelBar_menu(405,200,30,30,"rgba(139,69,19,",5,som);
			spArray.push(barra_som);

			//musica
			var botao_musica_mais = new Botao_menu(605,300,30,30,"rgba(139,69,19,","+");
			botao_musica_mais.nome = "+musica";
			spArray.push(botao_musica_mais);
			var botao_musica_menos = new Botao_menu(365,300,30,30,"rgba(139,69,19,","-");
			botao_musica_menos.nome = "-musica";
			spArray.push(botao_musica_menos);
			var barra_musica = new nivelBar_menu(405,300,30,30,"rgba(139,69,19,",5,musica);
			spArray.push(barra_musica);

			//dificuldade
			var botao_difculdade_mais = new Botao_menu(605,400,30,30,"rgba(139,69,19,","+");
			botao_difculdade_mais.nome = "+dif";
			spArray.push(botao_difculdade_mais);
			var botao_difculdade_menos = new Botao_menu(365,400,30,30,"rgba(139,69,19,","-");
			botao_difculdade_menos.nome = "-dif";
			spArray.push(botao_difculdade_menos);
			var barra_difculdade = new nivelBar_menu(405,400,30,30,"rgba(139,69,19,",5,dificuldade);
			spArray.push(barra_difculdade);

			var texto_som = new Texto(500,175,"SOM","#FFFFFF",20);
			spArray.push(texto_som);
			var texto_musica = new Texto(500,275,"MUSICA","#FFFFFF",20);
			spArray.push(texto_musica);
			var texto_dif = new Texto(500,375,"DIFICULDADE","#FFFFFF",20);
			spArray.push(texto_dif);

			var ev2 = new Event("comecar");
			ev2.spArray = spArray;
			ctx.canvas.dispatchEvent(ev2);
		}
		
	}

	draw()
	{
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			this.spArray[i].draw(this.ctx);
		}
	}
	adiciona(num){
		
		switch (num){
			case 0:
				this.spArray[4].adiciona();
			break;
			case 1:
				this.spArray[7].adiciona();
			break;
			case 2:
				this.spArray[10].adiciona();
			break;
		}
	}
	subtrai(num){
		switch (num){
			case 0:
				this.spArray[4].subtrai();
			break;
			case 1:
				this.spArray[7].subtrai();
			break;
			case 2:
				this.spArray[10].subtrai();
			break;
		}
	}

	//funcao responsavel por actualizar as posicoes
	render()
	{
		this.draw();
	}
	mouseMove(posX,posY){
		var teste = false;
		var dim = this.spArray.length;

		for (let i = 0; i < dim; i++){
			if(this.spArray[i].dentro(posX,posY)!=""){
				teste = true;
			}
		}

		if (teste){
			document.getElementById("canvas").style.cursor = "pointer";
		}else{
			document.getElementById("canvas").style.cursor = "initial";
		}
	}

	mouseClick(posX,posY){
		var botao = "";
		var dim = this.spArray.length;
		for (let i = 0; i < dim; i++){
			botao = this.spArray[i].dentro(posX,posY);
			if(botao != ""){
				break;
			}
		}
		if(botao=="X"){
			var ev3 = new Event("creditos_sair");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "+som"){
			var ev3 = new Event("som_mais");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "-som"){
			var ev3 = new Event("som_menos");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "+musica"){
			var ev3 = new Event("musica_mais");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "-musica"){
			var ev3 = new Event("musica_menos");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "+dif"){
			var ev3 = new Event("dificuldade_mais");
			this.ctx.canvas.dispatchEvent(ev3);
		}
		if(botao == "-dif"){
			var ev3 = new Event("dificuldade_menos");
			this.ctx.canvas.dispatchEvent(ev3);
		}
	}

}
	