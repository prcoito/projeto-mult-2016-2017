"use strict";
// onde estao as varias camadas do cenario
class botao_fase extends SpriteImage_menu
{

	constructor (x,y,cor,numero,activo)
	{
		super(x,y,100,100,null);
		this.cor = cor;
		this.numero = numero;
		this.activo = activo;
		this.estado = 0;

	}

	draw(ctx){
		if(this.activo){
			if(this.estado==0){
				ctx.fillStyle = this.cor+"0.5)";
				ctx.fillRect(this.x,this.y,this.width,this.height);
				ctx.fillStyle = "#EEEEEE"
				ctx.font = '70px verdana';
				ctx.textAlign = 'center';
				ctx.fillText(this.numero, this.x+this.width/2, this.y+this.height-20);
			}
			if(this.estado==1){

				ctx.fillStyle = this.cor+"0.9)";
				ctx.fillRect(this.x,this.y,this.width,this.height);
				ctx.fillStyle = "#EEEEEE"
				ctx.font = '70px verdana';
				ctx.textAlign = 'center';
				ctx.fillText(this.numero, this.x+this.width/2, this.y+this.height-20);
			}
		}else{
			ctx.fillStyle = this.cor+"0.3)";
			ctx.fillRect(this.x,this.y,this.width,this.height);
			ctx.fillStyle = "#CCCCCC"
			ctx.font = '70px verdana';
			ctx.textAlign = 'center';
			ctx.fillText(this.numero, this.x+this.width/2, this.y+this.height-20);
			ctx.font = '12px verdana';
			ctx.fillStyle = "#FFFFFF"
			ctx.fillText("BLOQUEADO",this.x+this.width/2,this.y+this.height-5);
		}
	}
	dentro(posX,posY){
		if(this.activo){
			if (this.x < posX  && this.x + this.width > posX &&
				this.y < posY && this.y + this.height > posY){
					this.estado = 1;			
					return this.numero+"_btn";
				}
			else{
				this.estado = 0;
				return "";
			}
		}else{
			return "";
		}
	}
}