"use strict";
// onde estao as varias camadas do cenario
class Texto extends SpriteImage_menu
{

	constructor (x,y,texto,cor,tamanho)
	{
		super(x,y,0,0,null);
		this.texto = texto;
		this.cor = cor;
		this.tamanho = tamanho;

	}

	draw(ctx){	
		ctx.fillStyle = this.cor;
		ctx.font = ""+this.tamanho+"px verdana";
		ctx.textAlign = 'center';
		ctx.fillText(this.texto, this.x, this.y+(this.tamanho/2));
	}
	dentro(posX,posY){
		return false;
	}
}